/*
QQE03-12 TPP
 Version 0.1.0
 Date: 14.09.2015
 Load with USBTinyISP and Original Mighty 1284P 16MHZ

 */

#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte QQE0312TPP_ID = 7;
static const byte ampId = QQE0312TPP_ID;

// Pin Config
#define relayPin 3
#define oneWireBusPin 4
#define reg6Pin 6 // 32KHZ
#define reg1Pin 7 // 32KHZ
#define reg4Pin 12 // 32KHZ
#define reg2Pin 13 // 32KHZ
#define reg3Pin 14 // 32KHZ
#define reg5Pin 15 // 32KHZ
#define phaseDetectionPin 16
#define ledOnBoardPin 21
#define ledPin 23

#define current1Pin A0
#define current3Pin A1
#define current2Pin A2
#define current4Pin A3
#define current6Pin A5
#define modulationPin A6
#define current5Pin A7

// Constants
#define startCurrent 30 // 12.45/mA
#define minCurrent 125 // 12.45/mA
#define maxCurrent 680 // 12.45/mA
#define minimalRefCurrent 187 // 12.45/mA
#define maximalRefCurrent 500 // 12.45/mA
#define dischargeMinTime 1 // Seconds
#define dischargeMaxTime 5 // Seconds
#define heatMaxTime 40 // Seconds
#define highVoltageMaxTime 10 // Seconds
#define regulationMaxTime 100 // Seconds
#define outOfRangeMaxTime 300 // Seconds
#define errorMaxTime 1000 // Milli-seconds
#define startingMasterP 0.2
#define startingMasterI 0.000125
#define startingSlaveP 0.4
#define startingSlaveI 0.00025
#define regulatingMasterP 0.06
#define regulatingMasterI 0.00006
#define regulatingSlaveP 0.03
#define regulatingSlaveI 0.00003
#define functionMasterP 0.03
#define functionMasterI 0.000003
#define functionSlaveP 0.015
#define functionSlaveI 0.00005
#define pidSampleTime 100
#define regulationTreshold 1 // 12.45/mA
#define functionTreshold 25 // 12.45/mA
#define currentAverageRatio 100
#define pidSetPointSlaveRatio 10
#define modulationPeakAverageRatio 1
#define modulationPeakReductionFactor 0.005
#define heatThinkTempMax 85 // > 85deg
#define airTempMax 70 // > 70deg
#define temperatureAverageRatio 5
#define phaseDetectionErrorMaxTime 100 // loop count, max 255
#define startingMinCurrent 100 // 12.45/mA
#define diagnosticSendMinTime 200 // Milli-seconds
#define tempMeasureMinTime 3000 // Milli-seconds

// Internal use
Blink ledOnBoard;
Blink led;
unsigned long lastDiagnosticTime = 0;
unsigned long lastTempMeasureTime = 0;
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
double current1Average = 0;
double current2Average = 0;
double current3Average = 0;
double current4Average = 0;
double current5Average = 0;
double current6Average = 0;
double modulationPeakAverage = 0;
double modulationPeak; //Initialized on reset()
double pid1Output; //Initialized on resetRegulators()
double pid2Output; //Initialized on resetRegulators()
double pid3Output; //Initialized on resetRegulators()
double pid4Output; //Initialized on resetRegulators()
double pid5Output; //Initialized on resetRegulators()
double pid6Output; //Initialized on resetRegulators()
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int airTemp = 0;
unsigned int grid2Reg1Temp = 0;
unsigned int grid2Reg2Temp = 0;
unsigned int anodeRegTemp = 0;
unsigned int powerSupplyTemp = 0;
unsigned long sequenceStartTime; //Initialized on reset()
unsigned long outOfRangeTime; //Initialized on reset()
unsigned long errorTime; //Initialized on reset()
byte phaseDetectionErrorCount = 0;
byte percentageSetPoint; //Initialized on reset()
double pidSetPoint; //Initialized on reset()
double pid2SetPoint; //Initialized on reset()
double pid4SetPoint; //Initialized on reset()
double pid6SetPoint; //Initialized on reset()
unsigned int currentMax;

// Init regulators
PID pid1(&current1Average, &pid1Output, &pidSetPoint, 0, 0, 0, 0, 255, pidSampleTime, false);
PID pid2(&current2Average, &pid2Output, &pid2SetPoint, 0, 0, 0, 0, 255, pidSampleTime, false);
PID pid3(&current3Average, &pid3Output, &pidSetPoint, 0, 0, 0, 0, 255, pidSampleTime, false);
PID pid4(&current4Average, &pid4Output, &pid4SetPoint, 0, 0, 0, 0, 255, pidSampleTime, false);
PID pid5(&current5Average, &pid5Output, &pidSetPoint, 0, 0, 0, 0, 255, pidSampleTime, false);
PID pid6(&current6Average, &pid6Output, &pid6SetPoint, 0, 0, 0, 0, 255, pidSampleTime, false);

// Timers definition
#define TIMER_CLK_DIV1 0x01 // Timer clocked at F_CPU
#define TIMER_PRESCALE_MASK 0x07

// Temp indexes in one wire bus
#define AIR_TEMPERATURE 4
#define G2REG1_TEMPERATURE 1
#define G2REG2_TEMPERATURE 2
#define ANODEREG_TEMPERATURE 0
#define POWERSUPPLY_TEMPERATURE 3

// Sequence:
#define SEQ_DISCHARGE 0 // 0: Discharge
#define SEQ_HEAT 1 // 1: Heat tempo
#define SEQ_STARTING 2 // 2: Starting High Voltage
#define SEQ_REGULATING 3 // 3: Waiting for reg
#define SEQ_FUNCTION 4 // 4: Normal Function
#define SEQ_FAIL 5 // 5: Fail
byte sequence = SEQ_DISCHARGE;

// Errors
#define NO_ERR 0 // No error
#define ERR_DISHARGETOOLONG 2 // 2: Discharge too long
#define ERR_CURRENTONHEAT 3 // 3: Current during heat time
#define ERR_STARTINGOUTOFRANGE 4 // 4: Out of range during starting
#define ERR_REGULATINGTOOLONG 5 // 5: Regulation too long
#define ERR_REGULATINGMAXREACHED 6 // 6: Maximun reached during regulation
#define ERR_REGULATINGMINREACHED 7 // 7: Minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED 8 // 8: Maximun reached during normal function
#define ERR_FUNCTIONMINREACHED 9 // 9: Minimum reached during normal function
#define ERR_FUNCTIONOUTOFRANGE 10 // 10: Time elapsed with current out of range during normal function
#define ERR_STARTINGTOOLONG 11 // 11: Starting too long
#define ERR_TEMPTOOHIGH 12 // 12: Temperature maximum reached
#define ERR_PHASE 14 // 14: Phase detection error
byte errorNumber = NO_ERR;

#define ERR_TUBE_1 1
#define ERR_TUBE_2 2
#define ERR_TUBE_3 3
#define ERR_TUBE_4 4
#define ERR_TUBE_5 5
#define ERR_TUBE_6 6
#define ERR_TUBE_7 7
#define ERR_TUBE_8 8
#define ERR_TEMP_AIR 1
#define ERR_TEMP_G2REG1 2
#define ERR_TEMP_G2REG2 3
#define ERR_TEMP_ANODEREG 4
#define ERR_TEMP_PWRSUPPLY 5
byte errorCause = NO_ERR;

#define CHECK_RANGE_OK 0
#define CHECK_RANGE_TOOLOW 1
#define CHECK_RANGE_TOOHIGH 2

// Diagnostic
EasyTransfer dataTx;
dataResponse dataTxStruct;

void reset() {
  sequenceStartTime = 0;
  outOfRangeTime = 0;
  errorTime = 0;
  modulationPeak = 0;
  percentageSetPoint = 0;
  pidSetPoint = minimalRefCurrent;
  pid2SetPoint = 0;
  pid4SetPoint = 0;
  pid6SetPoint = 0;
  resetRegulators();
}

void resetRegulators() {
  relayOff();

  pid1.SetEnabled(false);
  pid2.SetEnabled(false);
  pid3.SetEnabled(false);
  pid4.SetEnabled(false);
  pid5.SetEnabled(false);
  pid6.SetEnabled(false);

  pid1Output = 0;
  pid2Output = 0;
  pid3Output = 0;
  pid4Output = 0;
  pid5Output = 0;
  pid6Output = 0;

  analogWrite(reg1Pin, 0);
  analogWrite(reg2Pin, 0);
  analogWrite(reg3Pin, 0);
  analogWrite(reg4Pin, 0);
  analogWrite(reg5Pin, 0);
  analogWrite(reg6Pin, 0);
}

void initRegulators()
{
  if (sequence == SEQ_FUNCTION) {
    pid1.SetGains(functionMasterP, functionMasterI, 0);
    pid2.SetGains(functionSlaveP, functionSlaveI, 0);
    pid3.SetGains(functionMasterP, functionMasterI, 0);
    pid4.SetGains(functionSlaveP, functionSlaveI, 0);
    pid5.SetGains(functionMasterP, functionMasterI, 0);
    pid6.SetGains(functionSlaveP, functionSlaveI, 0);
  }
  else if (sequence == SEQ_STARTING) {
    pid1.SetGains(startingMasterP, startingMasterI, 0);
    pid2.SetGains(startingSlaveP, startingSlaveI, 0);
    pid3.SetGains(startingMasterP, startingMasterI, 0);
    pid4.SetGains(startingSlaveP, startingSlaveI, 0);
    pid5.SetGains(startingMasterP, startingMasterI, 0);
    pid6.SetGains(startingSlaveP, startingSlaveI, 0);
  }
  else {
    pid1.SetGains(regulatingMasterP, regulatingMasterI, 0);
    pid2.SetGains(regulatingSlaveP, regulatingSlaveI, 0);
    pid3.SetGains(regulatingMasterP, regulatingMasterI, 0);
    pid4.SetGains(regulatingSlaveP, regulatingSlaveI, 0);
    pid5.SetGains(regulatingMasterP, regulatingMasterI, 0);
    pid6.SetGains(regulatingSlaveP, regulatingSlaveI, 0);
  }

  pid1.SetEnabled(true);
  pid2.SetEnabled(true);
  pid3.SetEnabled(true);
  pid4.SetEnabled(true);
  pid5.SetEnabled(true);
  pid6.SetEnabled(true);
}

void computeRegulators()
{
  if (pid1.Compute()) {
    analogWrite(reg1Pin, constrain((int)pid1Output, 0, 255));
  }
  if (pid2.Compute()) {
    analogWrite(reg2Pin, constrain((int)pid2Output, 0, 255));
  }
  if (pid3.Compute()) {
    analogWrite(reg3Pin, constrain((int)pid3Output, 0, 255));
  }
  if (pid4.Compute()) {
    analogWrite(reg4Pin, constrain((int)pid4Output, 0, 255));
  }
  if (pid5.Compute()) {
    analogWrite(reg5Pin, constrain((int)pid5Output, 0, 255));
  }
  if (pid6.Compute()) {
    analogWrite(reg6Pin, constrain((int)pid6Output, 0, 255));
  }
}

byte checkInRange(double minValue, double maxValue)
{
  if (minValue > 0) {
    if (current1Average < minValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOLOW;
    }
    if (current2Average < minValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOLOW;
    }
    if (current3Average < minValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOLOW;
    }
    if (current4Average < minValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOLOW;
    }
    if (current5Average < minValue)
    {
      errorCause = ERR_TUBE_5;
      return CHECK_RANGE_TOOLOW;
    }
    if (current6Average < minValue)
    {
      errorCause = ERR_TUBE_6;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0) {
    if (current1Average > maxValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current2Average > maxValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current3Average > maxValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current4Average > maxValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current5Average > maxValue)
    {
      errorCause = ERR_TUBE_5;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current6Average > maxValue)
    {
      errorCause = ERR_TUBE_6;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int calcRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (current1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - current1Average) / range);
  }
  else if (maxValue > 0 && current1Average > maxValue)
  {
    percentProgress = 100 * (1 - (current1Average - maxValue) / range);
  }

  if (current2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current2Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current2Average - maxValue) / range), percentProgress);
  }

  if (current3Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current3Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current3Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current3Average - maxValue) / range), percentProgress);
  }

  if (current4Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current4Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current4Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current4Average - maxValue) / range), percentProgress);
  }

  if (current5Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current5Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current5Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current5Average - maxValue) / range), percentProgress);
  }

  if (current6Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current6Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current6Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current6Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

void sendDatas()
{
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(current1Average, 0, 1023, 0, 255); // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(current2Average, 0, 1023, 0, 255);
  dataTxStruct.measure2 = map(current3Average, 0, 1023, 0, 255);
  dataTxStruct.measure3 = map(current4Average, 0, 1023, 0, 255);
  dataTxStruct.measure4 = map(current5Average, 0, 1023, 0, 255);
  dataTxStruct.measure5 = map(current6Average, 0, 1023, 0, 255);
  dataTxStruct.measure6 = map(modulationPeak, 0, 1023, 0, 255);
  dataTxStruct.measure7 =  map(modulationPeakAverage, 0, 1023, 0, 255);
  dataTxStruct.output0 = constrain((int)pid1Output, 0, 255);
  dataTxStruct.output1 = constrain((int)pid2Output, 0, 255);
  dataTxStruct.output2 = constrain((int)pid3Output, 0, 255);
  dataTxStruct.output3 = constrain((int)pid4Output, 0, 255);
  dataTxStruct.output4 = constrain((int)pid5Output, 0, 255);
  dataTxStruct.output5 = constrain((int)pid6Output, 0, 255);
  dataTxStruct.output7 = constrain(airTemp, 0, 255);
  dataTxStruct.temperature0 = constrain(grid2Reg1Temp, 0, 255);
  dataTxStruct.temperature1 = constrain(grid2Reg2Temp, 0, 255);
  dataTxStruct.temperature2 = constrain(anodeRegTemp, 0, 255);
  dataTxStruct.temperature3 = constrain(powerSupplyTemp, 0, 255);
  dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(pidSetPoint, 0, 1023, 0, 255);
  dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();

  if (modulationPeak > modulationPeakReductionFactor ) {
    modulationPeak -= modulationPeakReductionFactor;
  }
  else {
    modulationPeak = 0;
  }

  lastDiagnosticTime = millis();
}

void measureTemperatures()
{
  // Send the command to get temperatures
  tempSensors.requestTemperatures();
  airTemp += (tempSensors.getTempCByIndex(AIR_TEMPERATURE) - airTemp) / temperatureAverageRatio;
  grid2Reg1Temp += (tempSensors.getTempCByIndex(G2REG1_TEMPERATURE)- grid2Reg1Temp) / temperatureAverageRatio;
  grid2Reg2Temp += (tempSensors.getTempCByIndex(G2REG2_TEMPERATURE)- grid2Reg2Temp) / temperatureAverageRatio;
  anodeRegTemp += (tempSensors.getTempCByIndex(ANODEREG_TEMPERATURE)- anodeRegTemp) / temperatureAverageRatio;
  powerSupplyTemp += (tempSensors.getTempCByIndex(POWERSUPPLY_TEMPERATURE)- powerSupplyTemp) / temperatureAverageRatio;
  lastTempMeasureTime = millis();
}

void relayOn() {
  analogWrite(relayPin, 127);
}

void relayOff() {
  analogWrite(relayPin, 0);
}

void regulate() {
  // Reset all elapsed time and force regulation
  sequenceStartTime = 0;
  sequence = SEQ_REGULATING;
}

void calcPercentSetPoint() {
  byte percentage = 0;

  if (modulationPeak > (percentageSetPoint == 100 ? 384 : 480)) {
    percentage = 100;
  }
  else if (modulationPeak > (percentageSetPoint >= 80 ? 240 : 300)) {
    percentage = 80;
  }
  else if (modulationPeak > (percentageSetPoint >= 60 ? 128 : 160)) {
    percentage = 60;
  }
  else if (modulationPeak > (percentageSetPoint >= 40 ? 48 : 60)) {
    percentage = 40;
  }
  else if (modulationPeak > (percentageSetPoint >= 20 ? 16 : 20)) {
    percentage = 20;
  }

  if (percentageSetPoint != percentage) {
    // New set point must be set
    pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent;
    percentageSetPoint = percentage;
    if (sequence >= SEQ_STARTING) {
      regulate();
    }
  }
}

void setup() {
  // initialize the digital pin as an output.
  pinMode(ledOnBoardPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(phaseDetectionPin, INPUT);

  // Set PWM speed to the maximum (32KHZ)
  TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR3B = (TCCR3B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

  digitalWrite(oneWireBusPin, HIGH);
  digitalWrite(phaseDetectionPin, HIGH);

  reset();

  ledOnBoard.Setup(ledOnBoardPin, false);
  led.Setup(ledPin, false);

  tempSensors.begin();
  tempSensors.requestTemperatures();
  measureTemperatures();

  // Diagnostic
  Serial.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);

  analogReference(INTERNAL1V1);
}

void loop()
{
  unsigned int elapsedTime;
  unsigned int check;
  unsigned long currentTime = millis();

  //checkPhase
  if (sequence != SEQ_FAIL && digitalRead(phaseDetectionPin) == HIGH) {
    phaseDetectionErrorCount++;
    if (phaseDetectionErrorCount > phaseDetectionErrorMaxTime) {
      // Fail, phase error
      sequence = SEQ_FAIL;
      errorNumber = ERR_PHASE;
    }
  }
  else {
    phaseDetectionErrorCount = 0;
  }

  if (sequence != SEQ_FAIL) {
    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_AIR;
    }
    else if (grid2Reg1Temp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_G2REG1;
    }
    else if (grid2Reg2Temp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_G2REG2;
    }
    else if (anodeRegTemp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_ANODEREG;
    }
    else if (powerSupplyTemp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PWRSUPPLY;
    }
    else {
      // Read and smooth the input
      unsigned int current1 = analogRead(current1Pin);
      unsigned int current2 = analogRead(current2Pin);
      unsigned int current3 = analogRead(current3Pin);
      unsigned int current4 = analogRead(current4Pin);
      unsigned int current5 = analogRead(current5Pin);
      unsigned int current6 = analogRead(current6Pin);

      current1Average += (current1 - current1Average) / currentAverageRatio;
      current2Average += (current2 - current2Average) / currentAverageRatio;
      current3Average += (current3 - current3Average) / currentAverageRatio;
      current4Average += (current4 - current4Average) / currentAverageRatio;
      current5Average += (current5 - current5Average) / currentAverageRatio;
      current6Average += (current6 - current6Average) / currentAverageRatio;

      if (sequence >= SEQ_REGULATING) {
        currentMax = max(max(max(current1, current2), max(current3, current4)), max(current5, current6));

        modulationPeakAverage += (analogRead(modulationPin) - modulationPeakAverage - 0.103 * currentMax) / modulationPeakAverageRatio;

        if (modulationPeak < modulationPeakAverage)
        {
          modulationPeak = modulationPeakAverage;
        }
      }

      // Calc regulators set point
      calcPercentSetPoint();

      // Average set points for slave regulators from master measure
      pid2SetPoint += (current1Average - pid2SetPoint) / pidSetPointSlaveRatio;
      pid4SetPoint += (current3Average - pid4SetPoint) / pidSetPointSlaveRatio;
      pid6SetPoint += (current5Average - pid6SetPoint) / pidSetPointSlaveRatio;
    }
  }

  switch (sequence)
  {
  case SEQ_DISCHARGE:
    // Discharging

    // Reset errors
    errorCause = NO_ERR;
    errorNumber = NO_ERR;

    // Pre-sequence
    if (sequenceStartTime == 0) {
      resetRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    ledOnBoard.Execute(800, 200);
    led.Execute(800, 200);

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if (elapsedTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    if (elapsedTime < dischargeMinTime || checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    sequence++;

  case SEQ_HEAT:
    // Startup tempo

    // Pre-sequence
    if (sequenceStartTime == 0) {
      resetRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    ledOnBoard.Execute(400, 400);
    led.Execute(400, 400);

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = elapsedTime;

    // Ensure no current at this step
    if (checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if (elapsedTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepElapsedTime = heatMaxTime;
    stepCurValue = heatMaxTime;

    // Post-sequence
    sequenceStartTime = 0;
    ledOnBoard.On();
    led.On();
    measureTemperatures();
    sendDatas();
    sequence++;
    delay(500);

  case SEQ_STARTING:
    // Starting High Voltage

    // Pre-sequence
    if (sequenceStartTime == 0) {
      relayOn();
      initRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    ledOnBoard.Execute(20, 400);
    led.Execute(20, 400);

    // Regulation
    computeRegulators();

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(startingMinCurrent, 0, startingMinCurrent);

    if (elapsedTime > highVoltageMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGOUTOFRANGE;
      break;
    }

    if (elapsedTime < 5)
    {
      break;
    }

    // If target points not reached, continue to regulate
    if (checkInRange(startingMinCurrent, 0) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sequenceStartTime = 0;
    sequence++;

  case SEQ_REGULATING:
    // Waiting for reg

    // Pre-sequence
    if (sequenceStartTime == 0) {
      relayOn();
      initRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    ledOnBoard.Execute(20, 1500);
    led.Execute(20, 1500);

    // Diagnostic
    stepMaxTime = regulationMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold, pidSetPoint - regulationTreshold);

    if (elapsedTime > regulationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      if (errorTime == 0) {
        errorTime = currentTime;
      }

      if (currentTime - errorTime > errorMaxTime)
      {
        // Fail current error
        sequence = SEQ_FAIL;
        errorNumber = ERR_REGULATINGMAXREACHED;
        break;
      }
    }
    else {
      errorTime = 0;
    }

    // If target points not reached, continue to regulate
    if (checkInRange(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    sequence++;

  case SEQ_FUNCTION:
    // Normal Function

    // Pre-sequence
    if (sequenceStartTime == 0) {
      relayOn();
      initRegulators();
      ledOnBoard.Off();
      led.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    // Measure Temp
    if (currentTime - lastTempMeasureTime > tempMeasureMinTime) {
      measureTemperatures();
    }

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 0;

    check = checkInRange(minCurrent, maxCurrent);
    if (check != CHECK_RANGE_OK)
    {
      if (errorTime == 0) {
        errorTime = currentTime;
      }

      if (currentTime - errorTime > errorMaxTime)
      {
        // Fail current error
        sequence = SEQ_FAIL;
        errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
        break;
      }
    }
    else {
      errorTime = 0;
    }

    if (checkInRange(pidSetPoint - functionTreshold, pidSetPoint + functionTreshold) != CHECK_RANGE_OK)
    {
      if (outOfRangeTime == 0) {
        outOfRangeTime = currentTime;
      }

      if ((currentTime - outOfRangeTime) / 1000 > outOfRangeMaxTime)
      {
        // Fail out of range error
        sequence = SEQ_FAIL;
        errorNumber = ERR_FUNCTIONOUTOFRANGE;
        break;
      }
    }
    else {
      outOfRangeTime = 0;
    }
    break;

  default:
    // Fail, protect mode
    reset();

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
    lastDiagnosticTime = 0;

    // Error indicator
    // Error indicator
    if (errorNumber == ERR_PHASE) {
      // Fast blinking on phase error (Stress blinking)
      ledOnBoard.Execute(80, 80);
      led.Execute(80, 80);
    }
    else {
      ledOnBoard.Execute(250, errorNumber, 1200);
      led.Execute(250, errorNumber, 1200);
    }
  }

  // Diagnostic
  if (currentTime - lastDiagnosticTime > diagnosticSendMinTime) {
    sendDatas();
  }
}






