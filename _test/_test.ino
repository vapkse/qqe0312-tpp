#define relayPin 3
#define oneWireBusPin 4 
#define reg6Pin 6 
#define reg1Pin 7 
#define reg4Pin 12 
#define reg2Pin 13 
#define reg3Pin 14 
#define reg5Pin 15 
#define tempPin 14
#define relayPin 15
#define phaseDetectionPin 16
#define ledOnBoardPin 21
#define ledRedPin 23
#define ledGreenPin 22

void reset(){
  relayOff();
}

void relayOn() {
  analogWrite(relayPin, 127);
}

void relayOff() {
  analogWrite(relayPin, 0);
}

void setup(){
  pinMode(ledOnBoardPin, OUTPUT);
  pinMode(ledRedPin, OUTPUT);
  pinMode(ledGreenPin, OUTPUT);
  pinMode(phaseDetectionPin, INPUT);
  digitalWrite(phaseDetectionPin, HIGH);   
}

void loop() {
  digitalWrite(ledOnBoardPin, HIGH);   

  for (int i=0; i<10; i++){
    digitalWrite(ledOnBoardPin, LOW); 
    delay(500);            
    digitalWrite(ledOnBoardPin, HIGH);   
    delay(500);              
  }

  analogWrite(reg1Pin, 0);
  analogWrite(reg2Pin, 0);
  analogWrite(reg3Pin, 0);
  analogWrite(reg4Pin, 0);
  analogWrite(reg5Pin, 0);
  analogWrite(reg6Pin, 0);
  relayOn();
  
  unsigned int reg1Value = 0;
  unsigned int reg2Value = 30;
  unsigned int reg3Value = 60;
  unsigned int reg4Value = 90;
  unsigned int reg5Value = 120;
  unsigned int reg6Value = 150;
  
  while (true){
    delay(10);
    if (reg1Value++ > 255){
      reg1Value = 0;
    }       
    if (reg2Value++ > 255){
      reg2Value = 0;
    }       
    if (reg3Value++ > 255){
      reg3Value = 0;
    }       
    if (reg4Value++ > 255){
      reg4Value = 0;
    }       
    if (reg5Value++ > 255){
      reg5Value = 0;
    }       
    if (reg6Value++ > 255){
      reg6Value = 0;
    }       
    analogWrite(reg1Pin, reg1Value);
    analogWrite(reg2Pin, reg2Value);
    analogWrite(reg3Pin, reg3Value);
    analogWrite(reg4Pin, reg4Value);
    analogWrite(reg5Pin, reg5Value);
    analogWrite(reg6Pin, reg6Value);
  }
}


