/*
 QQE03-12 TPP
 Version 2.5.3
 Date: 27.01.2017
 Authors: Da Silva Serge
 Load with USBTinyISP and Original Mighty 1284P 16MHZ
 ******************* WARNING *******************
 * The Rx pin must have a 1K pull-up resistor. *
 ***********************************************
 */

#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define QQE0312TPP_ID_L 131
#define QQE0312TPP_ID_R 132

static const uint8_t ampId = QQE0312TPP_ID_L;

// Constants
#define PARAMS_MEMORY_ADDRESS 0
#define PID_SAMPLE_TIME 100
#define REG_COUNT 6
#define MOD_LIMIT_COUNT 4
#define TEMP_COUNT 5

// Pin Config
#define rx 0
#define tx 1
#define relayPin 3
#define oneWireBusPin 4
#define phaseDetectionPin 16
#define ledOnBoardPin 21
#define ledPin 23
#define modulationPeakPin A6

uint8_t regPin[REG_COUNT] = {7, 13, 14, 12, 15, 6}; //  32KHZ
uint8_t measurePin[REG_COUNT] = {A0, A2, A1, A3, A7, A5};

uint8_t tempAddress_l[TEMP_COUNT] = {0, 1, 3, 2, 4};
uint8_t tempAddress_r[TEMP_COUNT] = {4, 1, 2, 0, 3};

struct DataResponse : DataResponseHeader {
    uint8_t minValue;
    uint8_t refValue;
    uint8_t maxValue;
    uint8_t output[REG_COUNT];
    uint8_t measure[REG_COUNT];
    uint8_t temp[TEMP_COUNT]; // Air, G2-1, G2-2, Anode, PS
    uint8_t modPeak;
    uint8_t modLimit[MOD_LIMIT_COUNT];
};

struct paramsResponse : SerialResponse {
    uint8_t workingPoint;
    uint8_t minWorkingPoint;
    uint8_t maxWorkingPoint;
    uint8_t minPoint;
    uint8_t maxPoint;
    uint8_t tempMeasureInterval;
    uint8_t tempAirMax;
    uint8_t tempRegulatorMax;
    uint8_t dischargeMinTime;
    uint8_t dischargeMaxTime;
    uint8_t heatMaxTime;
    uint8_t highVoltageMaxTime;
    uint8_t regulatedMinTime;
    uint8_t regulationMaxTime;
    uint8_t outOfRangeMaxTime;
    uint8_t errorMaxTime;
    uint8_t emergencyStopDelay;
    uint8_t startMasterP;
    uint8_t startMasterI;
    uint8_t startSlaveP;
    uint8_t startSlaveI;
    uint8_t regulationMasterP;
    uint8_t regulationMasterI;
    uint8_t regulationSlaveP;
    uint8_t regulationSlaveI;
    uint8_t functionMasterP;
    uint8_t functionMasterI;
    uint8_t functionSlaveP;
    uint8_t functionSlaveI;
    uint8_t regulationTreshold;
    uint8_t startingTreshold;
    uint8_t functionTreshold;
    uint8_t measureAverageRatio;
    uint8_t tempAverageRatio;
    uint8_t modulationPeakAverageRatio;
    uint8_t modulationPeakReductionFactor;
    uint8_t modulationPeakReductionTime;
    uint8_t modulationDetectedMinTime;
    uint8_t modulationCompensationFactor;
    uint8_t modulationLimit[MOD_LIMIT_COUNT];
    uint8_t phaseDetectionErrorMaxCount;
    uint8_t paramsFlags;
};

// Internal use
Blink ledOnBoard;
Blink led;
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
uint32_t lastTempMeasureTime;             // Initialized on reset()
double measureAverage[REG_COUNT];         //
double modulationPeakAverage = 0;         //
double modulationPeak;                    // Initialized on reset()
uint32_t modulationReductionLastTime = 0; // Initialized on reset()
uint32_t modulationGlitchFilterTime = 0;  //
double pidOutput[REG_COUNT];              // Initialized on resetRegulators()
uint8_t percentageWorkingPoint;           // Initialized on reset()
double pidWorkingPoint1;                  // Initialized on resetRegulators()
double pidWorkingPoint2;                  // Initialized on resetRegulators()
double pidMinPoint;                       //
double pidMaxPoint;                       //
uint8_t stepMaxTime = 0;                  //
uint16_t stepElapsedTime = 0;             //
uint8_t stepMaxValue = 0;                 //
uint16_t stepCurValue = 0;                //
double temp[TEMP_COUNT] = {};             // Initialized on reset() -> Air, G2-1, G2-2, Anode, PS
uint32_t sequenceStartTime;               // Initialized on reset()
uint32_t outOfRangeTime;                  // Initialized on reset()
uint32_t errorTime;                       // Initialized on reset()
uint32_t regulatedTime;                   // Initialized on reset()
uint32_t phaseDetectionErrorCount = 0;

// Init regulators
PID pid[REG_COUNT] = {
    PID(&measureAverage[0], &pidOutput[0], &pidWorkingPoint1, 0, 0, 0, 0, 255, PID_SAMPLE_TIME, false), PID(&measureAverage[1], &pidOutput[1], &pidWorkingPoint2, 0, 0, 0, 0, 255, PID_SAMPLE_TIME, false), PID(&measureAverage[2], &pidOutput[2], &pidWorkingPoint1, 0, 0, 0, 0, 255, PID_SAMPLE_TIME, false), PID(&measureAverage[3], &pidOutput[3], &pidWorkingPoint2, 0, 0, 0, 0, 255, PID_SAMPLE_TIME, false), PID(&measureAverage[4], &pidOutput[4], &pidWorkingPoint1, 0, 0, 0, 0, 255, PID_SAMPLE_TIME, false), PID(&measureAverage[5], &pidOutput[5], &pidWorkingPoint2, 0, 0, 0, 0, 255, PID_SAMPLE_TIME, false),
};

// Timers definition
#define TIMER_CLK_DIV1 0x01 // Timer clocked at F_CPU
#define TIMER_PRESCALE_MASK 0x07

// Temp indexes in one wire bus
uint8_t *tempAddress;

// Sequence
uint8_t sequence = SEQUENCE_DISCHARGE;

// Errors
uint8_t errorNumber = NO_ERROR;

#define ERROR_TUBE_1 1
#define ERROR_TUBE_2 2
#define ERROR_TUBE_3 3
#define ERROR_TUBE_4 4
#define ERROR_TUBE_5 5
#define ERROR_TUBE_6 6
#define ERROR_TEMP_AIR 1
#define ERROR_TEMP_G2REG1 2
#define ERROR_TEMP_G2REG2 3
#define ERROR_TEMP_ANODEREG 4
#define ERROR_TEMP_PS 5
uint8_t errorCause = NO_ERROR;

#define CHECK_RANGE_OK 0 // Careful Do not change, this values are used as int
#define CHECK_RANGE_TOOLOW 1
#define CHECK_RANGE_TOOHIGH 2

// Diagnostic, buffers allocation
SerialBuffer params;
paramsResponse *paramsBuffer = (paramsResponse *)params.alloc(sizeof(struct paramsResponse));

SerialBuffer response;
SerialResponse *responseBuffer = (SerialResponse *)response.alloc(sizeof(struct DataResponse));

SerialBuffer request;
SerialRequest *requestBuffer = (SerialRequest *)request.alloc(sizeof(struct SerialRequest));

void setDefaultParams() {
    params.reset();

    paramsBuffer->workingPoint = 18;                                 // %
    paramsBuffer->minWorkingPoint = 18;                              // %
    paramsBuffer->maxWorkingPoint = 50;                              // %
    paramsBuffer->minPoint = 50;                                     // %
    paramsBuffer->maxPoint = 50;                                     // %
    paramsBuffer->tempMeasureInterval = 10;                          // seconds
    paramsBuffer->tempAirMax = 70;                                   // Celsius
    paramsBuffer->tempRegulatorMax = 85;                             // Celsius
    paramsBuffer->dischargeMinTime = 1;                              // seconds
    paramsBuffer->dischargeMaxTime = 5;                              // Seconds
    paramsBuffer->heatMaxTime = 40;                                  // Seconds
    paramsBuffer->highVoltageMaxTime = 10;                           // Seconds
    paramsBuffer->regulationMaxTime = 30;                            // Seconds
    paramsBuffer->outOfRangeMaxTime = 5;                             // Minutes
    paramsBuffer->errorMaxTime = 10;                                 // Deciseconds
    paramsBuffer->regulatedMinTime = 2;                              // Seconds
    paramsBuffer->emergencyStopDelay = 50;                           // Milliseconds
    paramsBuffer->startMasterP = 225;                                // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->startMasterI = 151;                                // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->startSlaveP = 225;                                 // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->startSlaveI = 151;                                 // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->regulationMasterP = 213;                           // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->regulationMasterI = 144;                           // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->regulationSlaveP = 213;                            // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->regulationSlaveI = 144;                            // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->functionMasterP = 206;                             // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->functionMasterI = 114;                             // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->functionSlaveP = 200;                              // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->functionSlaveI = 142;                              // Scaled => 4/EXP((255-Value)/10)
    paramsBuffer->startingTreshold = 30;                             // mA
    paramsBuffer->regulationTreshold = 5;                            // mA
    paramsBuffer->functionTreshold = 25;                             // mA
    paramsBuffer->measureAverageRatio = 100;                         //
    paramsBuffer->tempAverageRatio = 3;                              //
    paramsBuffer->modulationPeakAverageRatio = 1;                    //
    paramsBuffer->modulationPeakReductionFactor = 188;               // Scaled
    paramsBuffer->modulationDetectedMinTime = 10;                    // Seconds
    paramsBuffer->modulationCompensationFactor = 80;                 //
    paramsBuffer->modulationLimit[0] = 13;                           // per 255
    paramsBuffer->modulationLimit[1] = 28;                           // per 255
    paramsBuffer->modulationLimit[2] = 56;                           // per 255
    paramsBuffer->modulationLimit[3] = 85;                           // per 255
    paramsBuffer->modulationPeakReductionTime = 200;                 // Milli-seconds
    paramsBuffer->phaseDetectionErrorMaxCount = 50;                  // loop count, max 255
    paramsBuffer->paramsFlags = REQUEST_PARAMSFLAG_WORKINGPOINTAUTO; //

    setWorkingPoint();
    setRegulatorsParams();
}

double getScaledParamFromByte(uint8_t value) { return 4 / exp((double)(255 - value) / 10); }

void reset() {
    resetModulationPeak();

    sequenceStartTime = 0;
    outOfRangeTime = 0;
    errorTime = 0;
    modulationReductionLastTime = 0;
    lastTempMeasureTime = millis();
    regulatedTime = 25;
    modulationPeak = 0;
    modulationReductionLastTime = 0;
    errorNumber = NO_ERROR;
    errorCause = NO_ERROR;
    percentageWorkingPoint = 0;
    sequence = SEQUENCE_DISCHARGE;

    for (uint8_t t = 0; t < TEMP_COUNT; t++) {
        temp[t] = 25;
    }

    resetRegulators();
}

void resetRegulators() {
    relayOff();

    setWorkingPoint();

    for (uint8_t r = 0; r < REG_COUNT; r++) {
        pid[r].SetEnabled(false);
        pidOutput[r] = 0;
        analogWrite(regPin[r], 0);
    }

    pidWorkingPoint1 = 0;
    pidWorkingPoint2 = 0;
}

void resetModulationPeak() { modulationPeak = 0; }

void setRegulatorsParams() {
    double masterP;
    double masterI;
    double slaveP;
    double slaveI;

    if (sequence == SEQUENCE_FUNCTION) {
        masterP = getScaledParamFromByte(paramsBuffer->functionMasterP);
        masterI = getScaledParamFromByte(paramsBuffer->functionMasterI);
        slaveP = getScaledParamFromByte(paramsBuffer->functionSlaveP);
        slaveI = getScaledParamFromByte(paramsBuffer->functionSlaveI);
    } else if (sequence == SEQUENCE_STARTING) {
        masterP = getScaledParamFromByte(paramsBuffer->startMasterP);
        masterI = getScaledParamFromByte(paramsBuffer->startMasterI);
        slaveP = getScaledParamFromByte(paramsBuffer->startSlaveP);
        slaveI = getScaledParamFromByte(paramsBuffer->startSlaveI);
    } else {
        masterP = getScaledParamFromByte(paramsBuffer->regulationMasterP);
        masterI = getScaledParamFromByte(paramsBuffer->regulationMasterI);
        slaveP = getScaledParamFromByte(paramsBuffer->regulationSlaveP);
        slaveI = getScaledParamFromByte(paramsBuffer->regulationSlaveI);
    }

    pid[0].SetGains(masterP, masterI, 0);
    pid[2].SetGains(masterP, masterI, 0);
    pid[4].SetGains(masterP, masterI, 0);
    pid[1].SetGains(slaveP, slaveI, 0);
    pid[3].SetGains(slaveP, slaveI, 0);
    pid[5].SetGains(slaveP, slaveI, 0);
}

void setWorkingPoint() {
    // Percent to 1024 scale
    uint32_t minPoint = (1023 * (uint32_t)paramsBuffer->minWorkingPoint) / 100;
    uint32_t maxPoint = (1023 * (uint32_t)paramsBuffer->maxWorkingPoint) / 100;
    uint32_t workingPoint = (1023 * (uint32_t)paramsBuffer->workingPoint) / 100;

    if (sequence == SEQUENCE_STARTING) {
        pidWorkingPoint1 = minPoint;
        if ((paramsBuffer->paramsFlags & REQUEST_PARAMSFLAG_WORKINGPOINTAUTO) == 0 && pidWorkingPoint1 > workingPoint) {
            // Manual mode
            pidWorkingPoint1 = workingPoint;
        }
        pidWorkingPoint2 = pidWorkingPoint1;
        pidMinPoint = 0;
        pidMaxPoint = pidWorkingPoint1 + ((1023 - pidWorkingPoint1) * (uint32_t)paramsBuffer->maxPoint) / 100;

    } else if (sequence == SEQUENCE_REGULATING || sequence == SEQUENCE_FUNCTION) {
        if ((paramsBuffer->paramsFlags & REQUEST_PARAMSFLAG_WORKINGPOINTAUTO) == 0) {
            // Manual mode
            pidWorkingPoint1 = workingPoint;
        } else {
            pidWorkingPoint1 = ((uint32_t)percentageWorkingPoint * (maxPoint - minPoint)) / 100 + minPoint;
        }
        pidWorkingPoint2 = pidWorkingPoint1;
        pidMinPoint = sequence == SEQUENCE_REGULATING ? 0 : (pidWorkingPoint1 * (uint32_t)paramsBuffer->minPoint) / 100;
        if (sequence == SEQUENCE_REGULATING) {
            double maxWorkingPoint = max(maxPoint, pidWorkingPoint1);
            pidMaxPoint = maxWorkingPoint + ((1023 - maxWorkingPoint) * (uint32_t)paramsBuffer->maxPoint) / 100;
        } else {
            pidMaxPoint = pidWorkingPoint1 + ((1023 - pidWorkingPoint1) * (uint32_t)paramsBuffer->maxPoint) / 100;
        }

    } else {
        pidWorkingPoint1 = 0;
        pidWorkingPoint2 = 0;
        pidMinPoint = 0;
        pidMaxPoint = sequence == SEQUENCE_HEAT ? min(workingPoint, minPoint) / 4 : 0;
    }
}

void initRegulators() {
    setWorkingPoint();
    setRegulatorsParams();
    for (uint8_t r = 0; r < REG_COUNT; r++) {
        pid[r].SetEnabled(true);
    }
}

void setParamsFlags(uint8_t flags) {
    // Filter available flags
    uint8_t oldflags = paramsBuffer->paramsFlags;
    uint8_t newflags = flags & REQUEST_PARAMSFLAG_WORKINGPOINTAUTO;

    if ((oldflags & REQUEST_PARAMSFLAG_WORKINGPOINTAUTO) != (newflags & REQUEST_PARAMSFLAG_WORKINGPOINTAUTO)) {
        setWorkingPoint();
        if (sequence == SEQUENCE_FUNCTION || sequence == SEQUENCE_REGULATING) {
            regulate();
        }
    }

    paramsBuffer->paramsFlags = newflags;
}

void computeRegulators() {
    for (uint8_t r = 0; r < REG_COUNT; r++) {
        if (pid[r].Compute()) {
            analogWrite(regPin[r], min((uint16_t)pidOutput[r], 255));
        }
    }
}

uint16_t checkInRange(double minValue, double maxValue) {
    for (uint8_t r = 0; r < REG_COUNT; r++) {
        if (minValue > 0) {
            if (measureAverage[r] < minValue) {
                errorCause = r + 1;
                return CHECK_RANGE_TOOLOW;
            }
        }

        if (maxValue > 0) {
            if (measureAverage[r] > maxValue) {
                errorCause = r + 1;
                return CHECK_RANGE_TOOHIGH;
            }
        }
    }

    errorCause = NO_ERROR;
    return CHECK_RANGE_OK;
}

uint16_t calcRegulationProgress(double minValue, double maxValue, double range) {
    double percentProgress = 100;

    for (uint8_t r = 0; r < REG_COUNT; r++) {
        if (measureAverage[r] < minValue) {
            percentProgress = min(100 * (1 - (minValue - measureAverage[r]) / range), percentProgress);
        } else if (measureAverage[r] > maxValue) {
            percentProgress = min(100 * (1 - (measureAverage[r] - maxValue) / range), percentProgress);
        }
    }

    return constrain((int)percentProgress, 0, 100);
}

void sendResponse(uint8_t message, uint8_t errorNumber, uint8_t extraValue) {
    responseBuffer->id = ampId;
    responseBuffer->msg = message;
    responseBuffer->errorNumber = errorNumber;
    responseBuffer->extraValue = extraValue;
    response.send(&Serial, sizeof(struct SerialResponse));
    Serial.flush();
}

void sendDatas() {
    // 0-60=seconds; > 60=minuts (minus 60); > 120=hours (minus 120)
    uint32_t tick = millis() / 1000;
    if (tick >= 3600) {
        tick = 119 + tick / 3600;
    } else if (tick >= 60) {
        tick = 59 + tick / 60;
    }

    // Send datas
    DataResponse *datasBuffer = (DataResponse *)responseBuffer;
    datasBuffer->id = ampId;
    datasBuffer->msg = RESPONSE_DATA;
    datasBuffer->errorNumber = errorNumber;
    datasBuffer->extraValue = errorCause;
    datasBuffer->step = sequence;
    datasBuffer->stepMaxTime = stepMaxTime;
    datasBuffer->stepElapsedTime = min(stepElapsedTime, 255);
    datasBuffer->stepMaxValue = stepMaxValue;
    datasBuffer->stepCurValue = min(stepCurValue, 255);
    datasBuffer->tickCount = min(tick, 255);
    datasBuffer->minValue = map((uint16_t)pidMinPoint, 0, 1023, 0, 255);
    datasBuffer->refValue = map((uint16_t)pidWorkingPoint1, 0, 1023, 0, 255);
    datasBuffer->maxValue = map(min(1023, (uint16_t)pidMaxPoint), 0, 1023, 0, 255);
    datasBuffer->modPeak = map((uint16_t)modulationPeak, 0, 1023, 0, 255);

    for (uint8_t r = 0; r < REG_COUNT; r++) {
        datasBuffer->output[r] = min((uint16_t)pidOutput[r], 255);
        datasBuffer->measure[r] = map((uint16_t)measureAverage[r], 0, 1023, 0, 255); // Input 1024 max, but I transfer only a range of 255
    }

    for (uint8_t l = 0; l < MOD_LIMIT_COUNT; l++) {
        datasBuffer->modLimit[l] = paramsBuffer->modulationLimit[l];
    }

    for (uint8_t t = 0; t < TEMP_COUNT; t++) {
        datasBuffer->temp[t] = temp[t];
    }

    response.send(&Serial, sizeof(struct DataResponse));
    Serial.flush();
}

void measureTemperatures() {
    // Send the command to get temperatures
    tempSensors.requestTemperatures();
    double ratio = (double)paramsBuffer->tempAverageRatio;
    for (uint8_t t = 0; t < TEMP_COUNT; t++) {
        temp[t] += (tempSensors.getTempCByIndex(*(tempAddress + t)) - temp[t]) / ratio;
    }
    lastTempMeasureTime = millis();
}

void relayOn() { analogWrite(relayPin, 127); }

void relayOff() { analogWrite(relayPin, 0); }

void regulate() {
    // Reset all elapsed time and force regulation
    sequenceStartTime = 0;
    sequence = SEQUENCE_REGULATING;
}

uint8_t calcPercentageWorkingPoint() {
    double limit = modulationPeak / 4; // 255 scale
    uint8_t percentStep = 100 / MOD_LIMIT_COUNT;
    uint8_t percent = 100;

    int l = MOD_LIMIT_COUNT;
    while (--l >= 0) {
        if (limit > paramsBuffer->modulationLimit[l]) {
            return percent;
        }
        percent -= percentStep;
    }

    return 0;
}

void setup() {
    // initialize the digital pin as an output.
    pinMode(ledOnBoardPin, OUTPUT);
    pinMode(ledPin, OUTPUT);
    pinMode(phaseDetectionPin, INPUT);

    reset();

    ledOnBoard.Setup(ledOnBoardPin, false);
    led.Setup(ledPin, false);

    // Diagnostic
    Serial.begin(2400);
    request.begin(&Serial);

    // Set PWM speed to the maximum (32KHZ)
    TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
    TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
    TCCR3B = (TCCR3B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

    analogReference(INTERNAL1V1);

    // Read eeprom params
    if (params.restore(PARAMS_MEMORY_ADDRESS) == 0) {
        setDefaultParams();
    }

    digitalWrite(oneWireBusPin, HIGH);
    digitalWrite(phaseDetectionPin, HIGH);

    // Map adrees of captors in the following index order
    // Air, G2-1, G2-2, Anode. PS
    tempAddress = (ampId == QQE0312TPP_ID_L) ? &tempAddress_l[0] : &tempAddress_r[0];

    tempSensors.begin();
    tempSensors.requestTemperatures();
    for (uint8_t i = 0; i < 5; i++) {
        measureTemperatures();
    }
}

void loop() {
    uint16_t elapsedTime = 0;
    uint32_t currentTime = millis();

    // checkPhase
    if (sequence != SEQUENCE_FAIL && digitalRead(phaseDetectionPin) == HIGH) {
        phaseDetectionErrorCount++;
        if (phaseDetectionErrorCount > paramsBuffer->phaseDetectionErrorMaxCount) {
            // Fail, phase error
            sequence = SEQUENCE_FAIL;
            errorNumber = ERROR_PHASE;
        }
    } else {
        phaseDetectionErrorCount = 0;
    }

    if (sequence != SEQUENCE_FAIL) {
        if (temp[0] > paramsBuffer->tempAirMax) {
            // Fail, air temperature too high
            sequence = SEQUENCE_FAIL;
            errorNumber = ERROR_TEMPTOOHIGH;
            errorCause = ERROR_TEMP_AIR;
        } else {
            for (uint8_t t = 1; t < TEMP_COUNT; t++) {
                if (temp[t] > paramsBuffer->tempRegulatorMax) {
                    // Fail, temperature too high
                    sequence = SEQUENCE_FAIL;
                    errorNumber = ERROR_TEMPTOOHIGH;
                    errorCause = t + 1;
                    break;
                }
            }
        }
    }

    if (sequence != SEQUENCE_FAIL) {
        // Read and smooth the input
        double ratio = (double)paramsBuffer->measureAverageRatio;
        uint16_t currentMax = 0;
        for (uint8_t r = 0; r < REG_COUNT; r++) {
            uint16_t measure = analogRead(measurePin[r]);
            measureAverage[r] += (measure - measureAverage[r]) / ratio;
            if (measure > currentMax) {
                currentMax = measure;
            }
        }

        if (sequence >= SEQUENCE_REGULATING) {
            double mod = analogRead(modulationPeakPin) - currentMax * (double)paramsBuffer->modulationCompensationFactor / 100;
            modulationPeakAverage += (max(mod, 0) - modulationPeakAverage) / (double)paramsBuffer->modulationPeakAverageRatio;

            if (modulationPeak < modulationPeakAverage) {
                // Two overshoot in modulationDetectedMinTime are required to boost
                // the current. Prevent glitch due to a sector pollution.
                if (currentTime < modulationGlitchFilterTime + 1000 * (uint32_t)paramsBuffer->modulationDetectedMinTime) {
                    modulationPeak = modulationPeakAverage;
                }

                modulationGlitchFilterTime = currentTime;
                if (modulationPeak < modulationPeakAverage) {
                    modulationPeakAverage = modulationPeak - 20;
                }
            }

            // Modulation reduction factor
            if (currentTime > modulationReductionLastTime + paramsBuffer->modulationPeakReductionTime) {
                double reductionFactor = getScaledParamFromByte(paramsBuffer->modulationPeakReductionFactor);
                if (modulationPeak > reductionFactor) {
                    modulationPeak -= reductionFactor;
                } else {
                    resetModulationPeak();
                }
                modulationReductionLastTime = currentTime;
            }
        }

        // Calc regulators set point
        if ((paramsBuffer->paramsFlags & REQUEST_PARAMSFLAG_WORKINGPOINTAUTO) != 0) {
            // Auto mode
            uint8_t percentage = calcPercentageWorkingPoint();
            if (percentageWorkingPoint != percentage) {
                // New set point must be set
                percentageWorkingPoint = percentage;
                setWorkingPoint();
                if (sequence >= SEQUENCE_STARTING) {
                    regulate();
                }
            }
        }

        uint16_t check = checkInRange(pidMinPoint, pidMaxPoint);
        if (check != CHECK_RANGE_OK) {
            if (errorTime == 0) {
                errorTime = currentTime;
            }

            if (currentTime > errorTime + 100 * (uint32_t)paramsBuffer->errorMaxTime) {
                // Fail voltage error
                sequence = SEQUENCE_FAIL;
                errorNumber = check == CHECK_RANGE_TOOLOW ? ERROR_MINCURRENTREACHED : ERROR_FUNCTIONMAXREACHED;
            }
        } else {
            errorTime = 0;
        }
    }

    if (sequence != SEQUENCE_STARTING && sequence != SEQUENCE_REGULATING && (currentTime - lastTempMeasureTime) / 1000 > paramsBuffer->tempMeasureInterval) {
        // Measure Temp
        measureTemperatures();
    }

    switch (sequence) {
    case SEQUENCE_DISCHARGE:
        // Discharging

        // Reset errors
        errorCause = NO_ERROR;
        errorNumber = NO_ERROR;

        // Pre-sequence
        if (sequenceStartTime == 0) {
            relayOff();
            resetRegulators();
            sequenceStartTime = currentTime;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        ledOnBoard.Execute(200, 800);
        led.Execute(200, 800);

        stepMaxTime = paramsBuffer->dischargeMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 0;
        stepCurValue = 1;

        if (elapsedTime > paramsBuffer->dischargeMaxTime) {
            // Fail, too late
            sequence = SEQUENCE_FAIL;
            errorNumber = ERROR_DISHARGETOOLONG;
            break;
        }

        // Percent to 1024 scale for minWorkingPoint
        if (elapsedTime < paramsBuffer->dischargeMinTime || checkInRange(0, (1023 * (uint32_t)min(paramsBuffer->workingPoint, paramsBuffer->minWorkingPoint)) / 400) == CHECK_RANGE_TOOHIGH) {
            break;
        }

        // Post-sequence
        sequenceStartTime = 0;
        sequence = SEQUENCE_HEAT;
        break;

    case SEQUENCE_HEAT:
        // Startup tempo

        // Pre-sequence
        if (sequenceStartTime == 0) {
            relayOff();
            resetRegulators();
            sequenceStartTime = currentTime;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        ledOnBoard.Execute(400, 400);
        led.Execute(400, 400);

        stepMaxTime = paramsBuffer->heatMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = paramsBuffer->heatMaxTime;
        stepCurValue = elapsedTime;

        if (elapsedTime < paramsBuffer->heatMaxTime) {
            break;
        }

        stepElapsedTime = paramsBuffer->heatMaxTime;
        stepCurValue = paramsBuffer->heatMaxTime;

        // Post-sequence
        sequenceStartTime = 0;
        sequence = SEQUENCE_STARTING;

        led.On();
        delay(250);
        break;

    case SEQUENCE_STARTING:
        // Starting High Voltage

        // Pre-sequence
        if (sequenceStartTime == 0) {
            relayOn();
            initRegulators();
            sequenceStartTime = currentTime;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        // Regulation
        computeRegulators();

        ledOnBoard.Execute(20, 400);
        led.Execute(20, 400);

        stepMaxTime = paramsBuffer->highVoltageMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 100;
        stepCurValue = calcRegulationProgress(pidWorkingPoint1 / 2, pidWorkingPoint1 + paramsBuffer->startingTreshold, pidWorkingPoint1 - paramsBuffer->startingTreshold);

        if (elapsedTime > paramsBuffer->highVoltageMaxTime) {
            // Fail, too late
            sequence = SEQUENCE_FAIL;
            errorNumber = ERROR_STARTINGTOOLONG;
            break;
        }

        // If target points not reached, continue to regulate
        if (checkInRange(pidWorkingPoint1 - paramsBuffer->startingTreshold, pidMaxPoint) != CHECK_RANGE_OK) {
            break;
        }

        // Post-sequence
        sequenceStartTime = 0;
        sequence = SEQUENCE_REGULATING;
        break;

    case SEQUENCE_REGULATING:
        // Waiting for reg

        // Pre-sequence
        if (sequenceStartTime == 0) {
            relayOn();
            initRegulators();
            sequenceStartTime = currentTime;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        // Regulation
        computeRegulators();

        ledOnBoard.Execute(20, 1500);
        led.Execute(20, 1500);

        stepMaxTime = paramsBuffer->regulationMaxTime;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 100;
        stepCurValue = calcRegulationProgress(pidWorkingPoint1 - paramsBuffer->regulationTreshold, pidWorkingPoint1 + paramsBuffer->regulationTreshold, paramsBuffer->startingTreshold - paramsBuffer->regulationTreshold);

        if (elapsedTime > paramsBuffer->regulationMaxTime) {
            // Fail, too late
            sequence = SEQUENCE_FAIL;
            errorNumber = ERROR_REGULATINGTOOLONG;
            break;
        }

        // If target points not reached, continue to regulate
        if (checkInRange(pidWorkingPoint1 - paramsBuffer->regulationTreshold, pidWorkingPoint1 + paramsBuffer->regulationTreshold) == CHECK_RANGE_OK) {
            if (regulatedTime == 0) {
                regulatedTime = currentTime;
            }

            if (currentTime < regulatedTime + 1000 * (uint32_t)paramsBuffer->regulatedMinTime) {
                break;
            }
        } else {
            regulatedTime = 0;
            break;
        }

        // Post-sequence
        sequenceStartTime = 0;
        sequence = SEQUENCE_FUNCTION;
        break;

    case SEQUENCE_FUNCTION:
        // Normal Function

        // Pre-sequence
        if (sequenceStartTime == 0) {
            relayOn();
            initRegulators();
            ledOnBoard.Off();
            led.Off();
            sequenceStartTime = currentTime;
            elapsedTime = 0;
        } else {
            // Calc elapsed time in seconds
            elapsedTime = (currentTime - sequenceStartTime) / 1000;
        }

        // Average set points for slave regulators from master measure
        pidWorkingPoint2 += (measureAverage[0] - pidWorkingPoint2) / (double)paramsBuffer->measureAverageRatio;

        // Regulation
        computeRegulators();

        stepMaxTime = 0;
        stepElapsedTime = elapsedTime;
        stepMaxValue = 0;
        stepCurValue = 0;

        if (checkInRange(pidWorkingPoint1 - paramsBuffer->functionTreshold, pidWorkingPoint1 + paramsBuffer->functionTreshold) != CHECK_RANGE_OK) {
            if (outOfRangeTime == 0) {
                outOfRangeTime = currentTime;
            }

            if (currentTime > outOfRangeTime + 60000 * (uint32_t)paramsBuffer->outOfRangeMaxTime) {
                // Fail out of range error
                sequence = SEQUENCE_FAIL;
                errorNumber = ERROR_FUNCTIONOUTOFRANGE;
                break;
            }
        } else {
            outOfRangeTime = 0;
        }
        break;

    default:
        // Fail, protect mode
        relayOff();

        stepMaxTime = 0;
        stepElapsedTime = 0;
        stepMaxValue = 0;
        stepCurValue = 0;

        // Error indicator
        if (errorNumber == ERROR_PHASE) {
            // Fast blinking on phase error (Stress blinking)
            ledOnBoard.Execute(80, 80);
            led.Execute(80, 80);
        } else {
            ledOnBoard.Execute(250, errorNumber, 1200);
            led.Execute(250, errorNumber, 1200);
        }
    }

    if (request.receive()) {
        switch (requestBuffer->msg) {
        // Command requests
        case REQUEST_RESET:
            reset();
            sendResponse(REQUEST_RESET, 0, 0);
            break;

        case REQUEST_STOP:
            sequence = SEQUENCE_FAIL;
            errorNumber = ERROR_EXTERNALSTOP;
            sendResponse(REQUEST_STOP, 0, 0);
            break;

        case REQUEST_WRITEPARAMS:
            if (params.store(PARAMS_MEMORY_ADDRESS) > 0) {
                sendResponse(REQUEST_WRITEPARAMS, 0, 0);
            } else {
                sendResponse(REQUEST_WRITEPARAMS, ERROR_WRITINGPARAMS, 0);
            }
            break;

        case REQUEST_RESETPARAMS:
            setDefaultParams();
            if (params.clearStored(PARAMS_MEMORY_ADDRESS)) {
                sendResponse(REQUEST_RESETPARAMS, 0, 0);
            } else {
                sendResponse(REQUEST_RESETPARAMS, ERROR_WRITINGPARAMS, 0);
            }
            break;

        case REQUEST_RESETMODULATION:
            resetModulationPeak();
            if (sequence == SEQUENCE_FUNCTION || sequence == SEQUENCE_REGULATING) {
                regulate();
            }
            sendResponse(REQUEST_RESETMODULATION, 0, 0);
            break;

        // Datas request
        case REQUEST_GETDATA:
            sendDatas();
            break;

        // Params requests
        case REQUEST_GETPARAMS:
            paramsBuffer->id = ampId;
            paramsBuffer->msg = REQUEST_GETPARAMS;
            params.send(&Serial, sizeof(struct paramsResponse));
            Serial.flush();
            break;

        case REQUEST_SETWORKINGPOINT:
            if ((paramsBuffer->paramsFlags & REQUEST_PARAMSFLAG_WORKINGPOINTAUTO) == 0) {
                // Manual mode
                paramsBuffer->workingPoint = requestBuffer->value;
                setWorkingPoint();
                if (sequence == SEQUENCE_FUNCTION || sequence == SEQUENCE_REGULATING) {
                    regulate();
                }
                sendResponse(REQUEST_SETWORKINGPOINT, 0, paramsBuffer->workingPoint);
            } else {
                sendResponse(REQUEST_SETWORKINGPOINT, ERROR_SETWORKINGPOINT, 0);
            }
            break;

        case REQUEST_SETMINWORKINGPOINT:
            paramsBuffer->minWorkingPoint = requestBuffer->value;
            setWorkingPoint();
            if (sequence == SEQUENCE_FUNCTION || sequence == SEQUENCE_REGULATING) {
                regulate();
            }
            sendResponse(REQUEST_SETMINWORKINGPOINT, 0, paramsBuffer->minWorkingPoint);
            break;

        case REQUEST_SETMAXWORKINGPOINT:
            paramsBuffer->maxWorkingPoint = requestBuffer->value;
            setWorkingPoint();
            if (sequence == SEQUENCE_FUNCTION || sequence == SEQUENCE_REGULATING) {
                regulate();
            }
            sendResponse(REQUEST_SETMAXWORKINGPOINT, 0, paramsBuffer->maxWorkingPoint);
            break;

        case REQUEST_SETMINPOINT:
            paramsBuffer->minPoint = requestBuffer->value;
            setWorkingPoint();
            sendResponse(REQUEST_SETMINPOINT, 0, paramsBuffer->minPoint);
            break;

        case REQUEST_SETMAXPOINT:
            paramsBuffer->maxPoint = requestBuffer->value;
            setWorkingPoint();
            sendResponse(REQUEST_SETMAXPOINT, 0, paramsBuffer->maxPoint);
            break;

        case REQUEST_SETTEMPMEASUREINTERVAL:
            paramsBuffer->tempMeasureInterval = requestBuffer->value;
            sendResponse(REQUEST_SETTEMPMEASUREINTERVAL, 0, paramsBuffer->tempMeasureInterval);
            break;

        case REQUEST_SETTEMPAIRMAX:
            paramsBuffer->tempAirMax = requestBuffer->value;
            sendResponse(REQUEST_SETTEMPAIRMAX, 0, paramsBuffer->tempAirMax);
            break;

        case REQUEST_SETTEMPREGULATORMAX:
            paramsBuffer->tempRegulatorMax = requestBuffer->value;
            sendResponse(REQUEST_SETTEMPREGULATORMAX, 0, paramsBuffer->tempRegulatorMax);
            break;

        case REQUEST_SETDISCHARGEMAXTIME:
            paramsBuffer->dischargeMaxTime = requestBuffer->value;
            sendResponse(REQUEST_SETDISCHARGEMAXTIME, 0, paramsBuffer->dischargeMaxTime);
            break;

        case REQUEST_SETHEATMAXTIME:
            paramsBuffer->heatMaxTime = requestBuffer->value;
            sendResponse(REQUEST_SETHEATMAXTIME, 0, paramsBuffer->heatMaxTime);
            break;

        case REQUEST_SETHIGHVOLTAGEMAXTIME:
            paramsBuffer->highVoltageMaxTime = requestBuffer->value;
            sendResponse(REQUEST_SETHIGHVOLTAGEMAXTIME, 0, paramsBuffer->highVoltageMaxTime);
            break;

        case REQUEST_SETREGULATIONMAXTIME:
            paramsBuffer->regulationMaxTime = requestBuffer->value;
            sendResponse(REQUEST_SETREGULATIONMAXTIME, 0, paramsBuffer->regulationMaxTime);
            break;

        case REQUEST_SETOUTOFRANGEMAXTIME:
            paramsBuffer->outOfRangeMaxTime = requestBuffer->value;
            sendResponse(REQUEST_SETOUTOFRANGEMAXTIME, 0, paramsBuffer->outOfRangeMaxTime);
            break;

        case REQUEST_SETERRORMAXTIME:
            paramsBuffer->errorMaxTime = requestBuffer->value;
            sendResponse(REQUEST_SETERRORMAXTIME, 0, paramsBuffer->errorMaxTime);
            break;

        case REQUEST_SETREGULATEDMINTIME:
            paramsBuffer->regulatedMinTime = requestBuffer->value;
            sendResponse(REQUEST_SETREGULATEDMINTIME, 0, paramsBuffer->regulatedMinTime);
            break;

        case REQUEST_SETEMERGENCYSTOPDELAY:
            paramsBuffer->emergencyStopDelay = requestBuffer->value;
            sendResponse(REQUEST_SETEMERGENCYSTOPDELAY, 0, paramsBuffer->emergencyStopDelay);
            break;

        case REQUEST_SETSTARTMASTERP:
            paramsBuffer->startMasterP = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETSTARTMASTERP, 0, paramsBuffer->startMasterP);
            break;

        case REQUEST_SETSTARTMASTERI:
            paramsBuffer->startMasterI = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETSTARTMASTERI, 0, paramsBuffer->startMasterI);
            break;

        case REQUEST_SETSTARTSLAVEP:
            paramsBuffer->startSlaveP = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETSTARTSLAVEP, 0, paramsBuffer->startSlaveP);
            break;

        case REQUEST_SETSTARTSLAVEI:
            paramsBuffer->startSlaveI = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETSTARTSLAVEI, 0, paramsBuffer->startSlaveI);
            break;

        case REQUEST_SETREGULATIONMASTERP:
            paramsBuffer->regulationMasterP = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETREGULATIONMASTERP, 0, paramsBuffer->regulationMasterP);
            break;

        case REQUEST_SETREGULATIONMASTERI:
            paramsBuffer->regulationMasterI = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETREGULATIONMASTERI, 0, paramsBuffer->regulationMasterI);
            break;

        case REQUEST_SETREGULATIONSLAVEP:
            paramsBuffer->regulationSlaveP = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETREGULATIONSLAVEP, 0, paramsBuffer->regulationSlaveP);
            break;

        case REQUEST_SETREGULATIONSLAVEI:
            paramsBuffer->regulationSlaveI = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETREGULATIONSLAVEI, 0, paramsBuffer->regulationSlaveI);
            break;

        case REQUEST_SETFUNCTIONMASTERP:
            paramsBuffer->functionMasterP = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETFUNCTIONMASTERP, 0, paramsBuffer->functionMasterP);
            break;

        case REQUEST_SETFUNCTIONMASTERI:
            paramsBuffer->functionMasterI = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETFUNCTIONMASTERI, 0, paramsBuffer->functionMasterI);
            break;

        case REQUEST_SETFUNCTIONSLAVEP:
            paramsBuffer->functionSlaveP = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETFUNCTIONSLAVEP, 0, paramsBuffer->functionSlaveP);
            break;

        case REQUEST_SETFUNCTIONSLAVEI:
            paramsBuffer->functionSlaveI = requestBuffer->value;
            setRegulatorsParams();
            sendResponse(REQUEST_SETFUNCTIONSLAVEI, 0, paramsBuffer->functionSlaveI);
            break;

        case REQUEST_SETREGULATIONTRESHOLD:
            paramsBuffer->regulationTreshold = requestBuffer->value;
            sendResponse(REQUEST_SETREGULATIONTRESHOLD, 0, paramsBuffer->regulationTreshold);
            break;

        case REQUEST_SETSTARTINGTRESHOLD:
            paramsBuffer->startingTreshold = requestBuffer->value;
            sendResponse(REQUEST_SETSTARTINGTRESHOLD, 0, paramsBuffer->startingTreshold);
            break;

        case REQUEST_SETFUNCTIONTRESHOLD:
            paramsBuffer->functionTreshold = requestBuffer->value;
            sendResponse(REQUEST_SETFUNCTIONTRESHOLD, 0, paramsBuffer->functionTreshold);
            break;

        case REQUEST_SETMEASUREAVERAGERATIO:
            paramsBuffer->measureAverageRatio = requestBuffer->value;
            sendResponse(REQUEST_SETMEASUREAVERAGERATIO, 0, paramsBuffer->measureAverageRatio);
            break;

        case REQUEST_SETTEMPAVERAGERATIO:
            paramsBuffer->tempAverageRatio = requestBuffer->value;
            sendResponse(REQUEST_SETTEMPAVERAGERATIO, 0, paramsBuffer->tempAverageRatio);
            break;

        case REQUEST_SETMODULATIONPEAKAVERAGERATIO:
            paramsBuffer->modulationPeakAverageRatio = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONPEAKAVERAGERATIO, 0, paramsBuffer->modulationPeakAverageRatio);
            break;

        case REQUEST_SETMODULATIONPEAKREDUCTIONFACTOR:
            paramsBuffer->modulationPeakReductionFactor = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONPEAKREDUCTIONFACTOR, 0, paramsBuffer->modulationPeakReductionFactor);
            break;

        case REQUEST_SETMODULATIONDETECTEDMINTIME:
            paramsBuffer->modulationDetectedMinTime = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONDETECTEDMINTIME, 0, paramsBuffer->modulationDetectedMinTime);
            break;

        case REQUEST_SETMODULATIONLIMIT25:
            paramsBuffer->modulationLimit[0] = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONLIMIT25, 0, paramsBuffer->modulationLimit[0]);
            break;

        case REQUEST_SETMODULATIONLIMIT50:
            paramsBuffer->modulationLimit[1] = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONLIMIT50, 0, paramsBuffer->modulationLimit[1]);
            break;

        case REQUEST_SETMODULATIONLIMIT75:
            paramsBuffer->modulationLimit[2] = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONLIMIT75, 0, paramsBuffer->modulationLimit[2]);
            break;

        case REQUEST_SETMODULATIONLIMIT100:
            paramsBuffer->modulationLimit[3] = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONLIMIT100, 0, paramsBuffer->modulationLimit[3]);
            break;

        case REQUEST_SETMODULATIONCOMPENSATIONFACTOR:
            paramsBuffer->modulationCompensationFactor = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONCOMPENSATIONFACTOR, 0, paramsBuffer->modulationCompensationFactor);
            break;

        case REQUEST_SETMODULATIONPEAKREDUCTIONTIME:
            paramsBuffer->modulationPeakReductionTime = requestBuffer->value;
            sendResponse(REQUEST_SETMODULATIONPEAKREDUCTIONTIME, 0, paramsBuffer->modulationPeakReductionTime);
            break;

        case REQUEST_SETDISCHARGEMINTIME:
            paramsBuffer->dischargeMinTime = requestBuffer->value;
            sendResponse(REQUEST_SETDISCHARGEMINTIME, 0, paramsBuffer->dischargeMinTime);
            break;

        case REQUEST_SETPHASEDETECTIONERRORMAXCOUNT:
            paramsBuffer->phaseDetectionErrorMaxCount = requestBuffer->value;
            sendResponse(REQUEST_SETPHASEDETECTIONERRORMAXCOUNT, 0, paramsBuffer->phaseDetectionErrorMaxCount);
            break;

        case REQUEST_SETPARAMSFLAGS:
            setParamsFlags(requestBuffer->value);
            sendResponse(REQUEST_SETPARAMSFLAGS, 0, paramsBuffer->paramsFlags);
            break;

        default:
            sendResponse(requestBuffer->msg, ERROR_UNKNOWNREQUEST, 0);
            break;
        }
    }
}
